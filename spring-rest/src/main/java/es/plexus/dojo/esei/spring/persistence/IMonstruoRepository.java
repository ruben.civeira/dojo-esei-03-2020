package es.plexus.dojo.esei.spring.persistence;

import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;
import es.plexus.dojo.esei.spring.model.Monstruo;

public interface IMonstruoRepository extends PagingAndSortingRepository<Monstruo, Long>{
  public Optional<Monstruo> findByNombre(String nombre);
}
