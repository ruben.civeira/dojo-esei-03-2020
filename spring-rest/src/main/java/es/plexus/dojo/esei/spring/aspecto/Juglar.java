package es.plexus.dojo.esei.spring.aspecto;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StopWatch;

@Aspect
@Configuration
public class Juglar {
  @Around("execution(* es.plexus.dojo.esei.spring.service.Cazador.cazar(..))")
  public Object cantaGesta(ProceedingJoinPoint joinPoint) throws Throwable {
    System.out.println("Vamos a iniciar una cacería");
    
    StopWatch stopWatch = new StopWatch();
    stopWatch.start();

    Object retVal = null;
    retVal = joinPoint.proceed();

    stopWatch.stop();
    System.out.println("\ttras " + stopWatch.getTotalTimeMillis() + "millis hemos perseguido a " + joinPoint.getArgs()[0] + (Boolean.TRUE.equals(retVal)?" y lo hemos cazado":" pero se ha escapado") );
    return retVal;
  }
}
