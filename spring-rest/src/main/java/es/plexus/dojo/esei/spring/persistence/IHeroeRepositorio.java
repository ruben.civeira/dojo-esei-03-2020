package es.plexus.dojo.esei.spring.persistence;

import org.springframework.data.repository.PagingAndSortingRepository;
import es.plexus.dojo.esei.spring.model.Heroe;

public interface IHeroeRepositorio extends PagingAndSortingRepository<Heroe, Long> {

}
