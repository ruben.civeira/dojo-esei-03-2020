package es.plexus.dojo.esei.spring.persistence;

import org.springframework.data.repository.PagingAndSortingRepository;
import es.plexus.dojo.esei.spring.model.Armamento;

public interface IArmamentoRepository extends PagingAndSortingRepository<Armamento, Long> {

}
