package es.plexus.dojo.esei.spring.persistence;

import org.springframework.data.repository.PagingAndSortingRepository;
import es.plexus.dojo.esei.spring.model.Familia;

public interface IFamiliaRepository extends PagingAndSortingRepository<Familia, Long>{

}
