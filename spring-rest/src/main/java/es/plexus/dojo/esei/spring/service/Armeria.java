package es.plexus.dojo.esei.spring.service;

import org.springframework.stereotype.Service;
import es.plexus.dojo.esei.spring.model.Armamento;

@Service
public class Armeria {
  private int fuerza = 25;
  
  public Armamento getArma() {
    Armamento a = new Armamento();
    a.setAtaque( fuerza );
    return a;
  }

  public void setFuerza(int fuerza) {
    this.fuerza = fuerza;
  }
}