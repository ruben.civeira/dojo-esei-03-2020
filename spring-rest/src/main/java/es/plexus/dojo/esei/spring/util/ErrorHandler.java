package es.plexus.dojo.esei.spring.util;

import java.util.NoSuchElementException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {
  @ExceptionHandler(NoSuchElementException.class)
  public ResponseEntity<String> handleNoFoundException(NoSuchElementException exception) {
    return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
  }
}
