package es.plexus.dojo.esei.spring.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import es.plexus.dojo.esei.spring.model.Monstruo;
import es.plexus.dojo.esei.spring.persistence.IMonstruoRepository;

@RestController
@RequestMapping(path = "/employees")
public class MonstruoConstroller {
  @Autowired
  private IMonstruoRepository monstruosRepository;
  
  @GetMapping
  public Page<Monstruo> list(@Valid Pageable pageable) {
    return monstruosRepository.findAll(pageable);
  }
  
  @PostMapping
  public Monstruo create(@RequestBody Monstruo monstruo) {
    return monstruosRepository.save(monstruo);
  }
  
  @PutMapping(path="{uid}")
  public Monstruo update(@PathVariable Long uid, @RequestBody Monstruo monstruo) {
    Monstruo original = monstruosRepository.findById(uid).get();
    original.setNombre( monstruo.getNombre() );
    original.setVida( monstruo.getVida() );
    original.setFamilia( monstruo.getFamilia() );
    return monstruosRepository.save(original);
  }
  
  @GetMapping(path="{uid}")
  public Monstruo retrieve(@PathVariable Long uid) {
    return monstruosRepository.findById(uid).get();
  }
}
