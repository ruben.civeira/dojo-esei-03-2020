package es.plexus.dojo.esei.spring.model;

public class Monstruo {
  private int vida;
  private String nombre;

  public int getVida() {
    return vida;
  }

  public void setVida(int vida) {
    this.vida = vida;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  
  @Override
  public String toString() {
    return nombre;
  }
}
