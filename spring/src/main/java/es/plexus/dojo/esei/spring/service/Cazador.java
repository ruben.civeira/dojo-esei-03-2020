package es.plexus.dojo.esei.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import es.plexus.dojo.esei.spring.model.Armamento;
import es.plexus.dojo.esei.spring.model.Monstruo;

@Service
public class Cazador {
  @Autowired
  private Armeria armeria;
  
  public boolean cazar(Monstruo monstruo) {
    Armamento arma = armeria.getArma();
    return arma.getAtaque() > monstruo.getVida(); 
  }
}
