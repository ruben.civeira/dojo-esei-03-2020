package es.plexus.dojo.esei.spring.model;

public class Armamento {
  private int ataque = 10;

  public int getAtaque() {
    return ataque;
  }

  public void setAtaque(int ataque) {
    this.ataque = ataque;
  }
}
