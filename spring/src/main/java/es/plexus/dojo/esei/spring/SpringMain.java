package es.plexus.dojo.esei.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import es.plexus.dojo.esei.spring.model.Monstruo;
import es.plexus.dojo.esei.spring.service.Cazador;

@SpringBootApplication
public class SpringMain implements CommandLineRunner {
  @Autowired
  private Cazador cazador;
  
  public static void main(String[] args) {
    SpringApplication app = new SpringApplication(SpringMain.class);
    app.setBannerMode(Banner.Mode.OFF);
    app.run(args);
  }

  @Override
  public void run(String... args) {
    Monstruo ms = new Monstruo();
    ms.setNombre("droconis");
    ms.setVida(22);
    for (int i = 0; i < args.length; ++i) {
      try {
        ms.setVida( Integer.parseInt(args[0]) );
      } catch(NumberFormatException nfe) {
        
      }
    }
    if( cazador.cazar(ms) ) {
      System.out.println("CAZADO");
    } else {
      System.err.println("Se esacpo");
    }
  }
}
