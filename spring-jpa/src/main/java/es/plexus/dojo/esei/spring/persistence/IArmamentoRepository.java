package es.plexus.dojo.esei.spring.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import es.plexus.dojo.esei.spring.model.Armamento;

public interface IArmamentoRepository extends JpaRepository<Armamento, Long> {

}
