package es.plexus.dojo.esei.spring.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import es.plexus.dojo.esei.spring.model.Familia;

public interface IFamiliaRepository extends JpaRepository<Familia, Long>{

}
