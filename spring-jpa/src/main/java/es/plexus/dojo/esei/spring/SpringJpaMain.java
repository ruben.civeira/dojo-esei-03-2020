package es.plexus.dojo.esei.spring;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import es.plexus.dojo.esei.spring.model.Monstruo;
import es.plexus.dojo.esei.spring.persistence.IMonstruoRepository;
import es.plexus.dojo.esei.spring.service.Cazador;
import es.plexus.dojo.esei.spring.service.Inicializador;

@SpringBootApplication
public class SpringJpaMain implements CommandLineRunner {
  @Autowired
  private Cazador cazador;
  @Autowired
  private Inicializador initializator;
  @Autowired
  private IMonstruoRepository monstruoRepository;
  
  public static void main(String[] args) {
    SpringApplication app = new SpringApplication(SpringJpaMain.class);
    app.setBannerMode(Banner.Mode.OFF);
    app.run(args);
  }

  @Override
  public void run(String... args) {
    if( !initializator.isInitialized() ) {
      initializator.initialize();
    }
    for (Monstruo monstruo : monstruoRepository.findAll()) {
      System.out.println(": TENGO UN monstruo " + monstruo);
    }
    Optional<Monstruo> oms = monstruoRepository.findByNombre("dragon rojo");
    if( oms.isPresent() ) {
      Monstruo ms = oms.get();
      if( cazador.cazar(ms) ) {
        System.out.println("CAZADO");
      } else {
        System.err.println("Se esacpo");
      }
    }
  }
}
