package es.plexus.dojo.esei.spring.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import es.plexus.dojo.esei.spring.model.Heroe;

public interface IHeroeRepositorio extends JpaRepository<Heroe, Long>{

}
