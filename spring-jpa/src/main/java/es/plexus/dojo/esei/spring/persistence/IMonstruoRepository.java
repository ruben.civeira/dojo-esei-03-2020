package es.plexus.dojo.esei.spring.persistence;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import es.plexus.dojo.esei.spring.model.Monstruo;

public interface IMonstruoRepository extends JpaRepository<Monstruo, Long>{
  public Optional<Monstruo> findByNombre(String nombre);
}
