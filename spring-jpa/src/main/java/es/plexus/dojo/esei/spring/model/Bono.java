package es.plexus.dojo.esei.spring.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Bono {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  
  private int extra;
  
  @ManyToOne(optional = true, fetch = FetchType.LAZY)
  private Heroe heroe;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getExtra() {
    return extra;
  }

  public void setExtra(int extra) {
    this.extra = extra;
  }

  public Heroe getHeroe() {
    return heroe;
  }

  public void setHeroe(Heroe heroe) {
    this.heroe = heroe;
  }
}
