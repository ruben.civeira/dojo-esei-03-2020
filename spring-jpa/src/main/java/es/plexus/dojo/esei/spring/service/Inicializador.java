package es.plexus.dojo.esei.spring.service;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import es.plexus.dojo.esei.spring.model.Armamento;
import es.plexus.dojo.esei.spring.model.Bono;
import es.plexus.dojo.esei.spring.model.Familia;
import es.plexus.dojo.esei.spring.model.Heroe;
import es.plexus.dojo.esei.spring.model.Monstruo;
import es.plexus.dojo.esei.spring.persistence.IArmamentoRepository;
import es.plexus.dojo.esei.spring.persistence.IFamiliaRepository;
import es.plexus.dojo.esei.spring.persistence.IHeroeRepositorio;
import es.plexus.dojo.esei.spring.persistence.IMonstruoRepository;

@Service
public class Inicializador {
  @Autowired
  private IMonstruoRepository monstruoRepositorio;
  @Autowired
  private IHeroeRepositorio heroeRepositorio;
  @Autowired
  private IFamiliaRepository familiaRepository;
  @Autowired
  private IArmamentoRepository armamentoRepository;
  
  public boolean isInitialized() {
    return 0 != monstruoRepositorio.count();
  }
  
  public void initialize() {
    initHeroe();
    initArmas();
    initMostruos();
  }
  
  private void initMostruos() {
    Familia fd = new Familia();
    fd.setNombre("dracos");
    familiaRepository.save( fd );
    
    Familia fe = new Familia();
    fe.setNombre("esqueletos");
    familiaRepository.save( fe );
    
    Monstruo m1 = new Monstruo();
    m1.setNombre("dragon rojo");
    m1.setFamilia( fd );
    m1.setVida( 10 );
    monstruoRepositorio.save(m1);
    
    Monstruo m2 = new Monstruo();
    m2.setNombre("dragon verde");
    m2.setFamilia( fd );
    m2.setVida( 12 );
    monstruoRepositorio.save(m2);
    
    Monstruo e1 = new Monstruo();
    e1.setNombre("esqueleto");
    e1.setFamilia( fe );
    e1.setVida( 5 );
    monstruoRepositorio.save(e1);
  }
  
  private void initArmas() {
    Armamento a1 = new Armamento();
    a1.setNombre("Arco");
    a1.setAtaque(10);
    armamentoRepository.save( a1 );
    
    Armamento a2 = new Armamento();
    a2.setNombre("Ballesta");
    a2.setAtaque(15);
    armamentoRepository.save( a2 );
  }
  
  private void initHeroe() {
    Heroe h = new Heroe();
    h.setName("Juanillo");
    h.setBonos( new ArrayList<>() );
    Bono b1 = new Bono();
    b1.setHeroe( h );
    b1.setExtra( 10 );
    h.getBonos().add( b1 );
    
    Bono b2 = new Bono();
    b2.setHeroe( h );
    b2.setExtra( 4 );
    h.getBonos().add( b2 );
    
    heroeRepositorio.save( h );
  }
}
