package es.plexus.dojo.esei.spring.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Monstruo {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  private int vida;
  private String nombre;
  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  private Familia familia;

  public int getVida() {
    return vida;
  }

  public void setVida(int vida) {
    this.vida = vida;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  
  @Override
  public String toString() {
    return nombre;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Familia getFamilia() {
    return familia;
  }

  public void setFamilia(Familia familia) {
    this.familia = familia;
  }
}
